﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
 
public class BundleWebLoader : MonoBehaviour
{
    public string bundleUrl = "http://localhost:8000/";
    public string assetName = "xyz"; // it can be asset name, asset name with extension even with asset absolute path and name


    // Start is called before the first frame update
    IEnumerator Start()
    {
        //----------Obsolate-----------
        //using (WWW web = new WWW(bundleUrl))
        //{
        //    yield return web;
        //    AssetBundle remoteAssetBundle = web.assetBundle;
        //    if (remoteAssetBundle == null)
        //    {
        //        Debug.LogError("Failed to download AssetBundle!");
        //        yield break;
        //    }
        //    Instantiate(remoteAssetBundle.LoadAsset(assetName));
        //    remoteAssetBundle.Unload(false);
        //}

        UnityWebRequest unityWebRequest = UnityWebRequestAssetBundle.GetAssetBundle(bundleUrl);
        yield return unityWebRequest.SendWebRequest();

        AssetBundle remoteAssetBundle = DownloadHandlerAssetBundle.GetContent(unityWebRequest);
        var loadAsset = remoteAssetBundle.LoadAssetAsync<GameObject>(assetName);
        //var loadAsset = remoteAssetBundle.LoadAsset(assetName);
        yield return loadAsset;

        Instantiate(loadAsset.asset);
    }

}


